package me.rushmead.doomshrine.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import me.rushmead.doomshrine.DoomShrine;
import me.rushmead.doomshrine.TileDoomShrine;
import me.rushmead.doomshrine.gui.GuiDoomShrine;
import me.rushmead.doomshrine.handlers.TickHandler;
import me.rushmead.doomshrine.renderers.RenderDoomShrine;
import me.rushmead.doomshrine.renderers.RenderDoomShrineItem;

/**
 * Created by Rushmead for StageCraftTesting
 */
public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRenders()
    {
    	ClientRegistry.bindTileEntitySpecialRenderer(TileDoomShrine.class, new RenderDoomShrine());
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(DoomShrine.doomShrineBlock), new RenderDoomShrineItem(new RenderDoomShrine(), new TileDoomShrine()));
    }

    @Override
    public void registerHandlers() {
    	
    	MinecraftForge.EVENT_BUS.register(new GuiDoomShrine(Minecraft.getMinecraft()));

    }
    
}