package me.rushmead.doomshrine.proxy;

import net.minecraft.entity.player.EntityPlayer;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

/**
 * Created by Rushmead for StageCraftTesting
 */
public interface IProxy {
    public void registerRenders();

    public void registerHandlers();
    
}
