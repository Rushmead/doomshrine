package me.rushmead.doomshrine.proxy;

import me.rushmead.doomshrine.gui.GuiDoomShrine;
import me.rushmead.doomshrine.handlers.TickHandler;
import me.rushmead.doomshrine.util.Log;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

/**
 * Created by Rushmead for StageCraftTesting
 */
public class ServerProxy extends CommonProxy
{
    @Override
    public void registerRenders()
    {
        // NOOP
    }

    @Override
    public void registerHandlers() {
    	// NOOP
    }
    
}