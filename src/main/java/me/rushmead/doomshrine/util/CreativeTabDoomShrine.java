package me.rushmead.doomshrine.util;


import me.rushmead.doomshrine.DoomShrine;
import me.rushmead.doomshrine.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

/**
 * Created by Rushmead for StageCraftTesting
 */
public class CreativeTabDoomShrine extends CreativeTabs{


    public CreativeTabDoomShrine() {
		super(Reference.MODID);
	}
    
    public Item getTabIconItem()
    {
    	return Item.getItemFromBlock(DoomShrine.doomShrineBlock);
    }
}
