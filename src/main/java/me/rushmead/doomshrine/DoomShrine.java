package me.rushmead.doomshrine;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import me.rushmead.doomshrine.blocks.BlockDoomShrine;
import me.rushmead.doomshrine.gui.GuiDoomShrine;
import me.rushmead.doomshrine.handlers.EventHandler;
import me.rushmead.doomshrine.handlers.TickHandler;
import me.rushmead.doomshrine.packets.KillPlayer;
import me.rushmead.doomshrine.proxy.IProxy;
import me.rushmead.doomshrine.util.CreativeTabDoomShrine;
import me.rushmead.doomshrine.util.Log;

/**
 * Created by Rushmead for StageCraftTesting
 */
@Mod(modid = Reference.MODID, version = Reference.VERSION)
public class DoomShrine {
	
	//Declare Blocks & Items
    public static Block doomShrineBlock;
    
    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    public static IProxy proxy;
  
    
    //Creative Tab
    public static CreativeTabs tabdoomshrine = new CreativeTabDoomShrine();
    
    public static SimpleNetworkWrapper network;
    
    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
    	//Register Handlers
    	TickHandler tickHandler = new TickHandler();
        FMLCommonHandler.instance().bus().register(tickHandler);
        Log.info("Just Registed the TickHandler in preinit!");
        
    	EventHandler eventHandler = new EventHandler();
        MinecraftForge.EVENT_BUS.register(eventHandler);
        FMLCommonHandler.instance().bus().register(eventHandler);
        Log.info("Just Registed the EventHandler in preinit!");
        
        proxy.registerHandlers();
    }
    
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){

        ConfigurationHandler.init(event.getSuggestedConfigurationFile());
        FMLCommonHandler.instance().bus().register(new ConfigurationHandler());
        Log.info("Configs All done!");
        network = NetworkRegistry.INSTANCE.newSimpleChannel("MyChannel");
        network.registerMessage(KillPlayer.Handler.class, KillPlayer.class, 0, Side.SERVER);
        Log.info("Packets Registered.");
        
        doomShrineBlock = new BlockDoomShrine();
        
    	GameRegistry.registerTileEntity(TileDoomShrine.class, "TileDoomShrine");
    	GameRegistry.registerBlock(doomShrineBlock, "DoomShrine");
    }
    
    @Mod.EventHandler 
    public void load(FMLInitializationEvent event) {
    	proxy.registerRenders();
    	addNames();
             
    }
    
    //Temporary Naming
    public static void addNames(){
        LanguageRegistry.addName(doomShrineBlock, "Doom Shrine");
        LanguageRegistry.instance().addStringLocalization("itemGroup.doomshrine", "en_US", "Doom Shrine");
}
}
