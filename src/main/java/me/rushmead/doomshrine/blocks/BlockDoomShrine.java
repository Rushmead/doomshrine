package me.rushmead.doomshrine.blocks;

import java.util.ArrayList;

import me.rushmead.doomshrine.DoomShrine;
import me.rushmead.doomshrine.TileDoomShrine;
import me.rushmead.doomshrine.util.CreativeTabDoomShrine;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Created by Rushmead for Doom Shrine
 */
public class BlockDoomShrine extends BlockContainer {
	
	private ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
    public BlockDoomShrine() {
        super(Material.rock);
        this.setBlockUnbreakable();
        setBlockName("DoomShrine");
        setCreativeTab(DoomShrine.tabdoomshrine);
    }

    @Override
	public boolean isOpaqueCube()
	{
	 return false;
	}
	
    @Override
    public boolean hasTileEntity()
    {
        return true;
    }  
    
	@Override
	public TileEntity createNewTileEntity(World world, int metadata) {
		return new TileDoomShrine();
	}
	
	@Override
    public boolean renderAsNormalBlock() {
            return false;
    }
	

    @Override
    public int getRenderType()
    {
        return -1;
    }
    
	@Override
	public void registerBlockIcons(IIconRegister icon){
            this.blockIcon = icon.registerIcon("doomshrine:DoomShrine");
    }

    
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9){
		
		TileDoomShrine te = (TileDoomShrine) world.getTileEntity(x, y, z);
		
		 if (te == null || player.isSneaking()){
	            return false;
	     }
		 
		 ItemStack playerItem = player.getCurrentEquippedItem();
		 
		 if (te.getStackInSlot(0) == null && playerItem != null)
	        {
	            ItemStack newItem = playerItem.copy();
	            newItem.stackSize = 1;
	            --playerItem.stackSize;
	            te.setInventorySlotContents(0, newItem);
	     }else if (te.getStackInSlot(0) != null && playerItem == null)
	     {
	            player.inventory.addItemStackToInventory(te.getStackInSlot(0));
	            te.setInventorySlotContents(0, null);
	     }
		 
		 world.markBlockForUpdate(x, y, z);
		return true;
		
	} 

	
	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z, boolean willHarvest) {
		
		final TileDoomShrine te = (TileDoomShrine) world.getTileEntity(x, y, z);
			if (te.getStackInSlot(0) != null){
				ItemStack itemstack = te.getStackInSlot(0);
				ret.add(itemstack);
			}
		return super.removedByPlayer(world, player, x, y, z, willHarvest);
	}
}
