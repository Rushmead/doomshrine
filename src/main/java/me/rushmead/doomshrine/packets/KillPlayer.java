package me.rushmead.doomshrine.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * /**
 * File by Rushmead for doomshrine
 */

public class KillPlayer implements IMessage {

    public static String player;

    public KillPlayer() { }

    public KillPlayer(String text) {
        this.player = text;
    }
    @Override
    public void fromBytes(ByteBuf buf) {
        player = ByteBufUtils.readUTF8String(buf); // this class is very useful in general for writing more complex objects
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, player);
    }
    public static class Handler implements IMessageHandler<KillPlayer, IMessage> {

        @Override
        public IMessage onMessage(KillPlayer message, MessageContext ctx) {

           if(message.player != null){
               ArrayList<EntityPlayerMP> allp = new ArrayList<EntityPlayerMP>();
               ListIterator itl;

               for(int i = 0; i<MinecraftServer.getServer().worldServers.length; i++) {
                   itl = MinecraftServer.getServer().worldServers[i].playerEntities.listIterator();
                   while(itl.hasNext()) allp.add((EntityPlayerMP)itl.next());
               }
               for(EntityPlayerMP player: allp){
                   if(player.getUniqueID().toString() == message.player){
                       player.setHealth(0);
                   }
               }
           }
            return null; // no response in this case
        }
    }
}
