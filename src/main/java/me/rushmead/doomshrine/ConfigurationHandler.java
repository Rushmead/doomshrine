package me.rushmead.doomshrine;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

/**
 * Created by Rushmead for Doom Shrine
 */
public class ConfigurationHandler
{
            public static Configuration configuration;
            public static String ClockLocation = "";
        public static int defaultTimeInTicks = 12000;
            public static void init(File configFile)
            {
                // Create the configuration object from the given configuration file
                if (configuration == null)
                {
                    configuration = new Configuration(configFile);
                    loadConfiguration();
                }
            }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event)

            {
                if (event.modID.equalsIgnoreCase(Reference.MODID))
                {
                    loadConfiguration();
                }
            }
        
            public static void loadConfiguration()
            {
                ClockLocation = configuration.getString("clockWhere", Configuration.CATEGORY_GENERAL, "topLeft", "Location Of The Clock on the Screen eg. topLeft");
                defaultTimeInTicks = configuration.getInt("amountOfTime", Configuration.CATEGORY_GENERAL, 10, 0, 120, "Amount of Starting Time in Minutes") * 60 *20;
                if (configuration.hasChanged())
                {
                    configuration.save();
                }
            }
        }