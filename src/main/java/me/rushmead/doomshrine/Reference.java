package me.rushmead.doomshrine;

/**
 * Created by Rushmead for StageCraftTesting
 */
public class Reference {
    public static final String MODID = "doomshrine";
    public static final String MOD_NAME = "Doom Shrine";
    public static final String VERSION = "1.7.10-1.0";
    public static final String CLIENT_PROXY_CLASS = "me.rushmead.doomshrine.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "me.rushmead.doomshrine.proxy.ServerProxy";

}
