package me.rushmead.doomshrine.handlers;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import me.rushmead.doomshrine.DoomShrine;
import me.rushmead.doomshrine.entity.ExtendedPlayerData;
import me.rushmead.doomshrine.packets.KillPlayer;
import me.rushmead.doomshrine.util.Log;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraftforge.event.entity.player.PlayerEvent;

import java.awt.*;
import java.util.Calendar;

import org.lwjgl.opengl.GL11;

public class TickHandler {

    public static boolean stop = false;
    public static long timeInMili = 0;	
    
    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
    	
      if (event.phase == TickEvent.Phase.END) {
    	  if (event.player !=  null){
    		  
    		  ExtendedPlayerData properties = ExtendedPlayerData.get(event.player);
    		  
    		  if (!stop) {
    			  
    			  	timeInMili++;
    			  	
                	if(timeInMili > 20){
                		if (properties.decreaseShrineTimer(1)){
                			timeInMili = 0;
                		}
                	}
                	
                if (properties.getShrineTimer() <= 0) {
                    stop = true;
                }
                
            if (stop) {
            		event.player.worldObj.spawnEntityInWorld(new EntityLightningBolt(event.player.worldObj, event.player.posX, event.player.posY, event.player.posZ));
            		event.player.worldObj.createExplosion(event.player, event.player.posX, event.player.posY, event.player.posZ, 1, false);
                    event.player.setHealth(0);
                    DoomShrine.network.sendToServer(new KillPlayer(event.player.getUniqueID().toString()));
                    stop = false;
            }
            }
          }
    	}
      }
}