package me.rushmead.doomshrine.entity;

import me.rushmead.doomshrine.ConfigurationHandler;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class ExtendedPlayerData implements IExtendedEntityProperties {
	
	private final EntityPlayer entplayer;
	private int maxTime, ShrineTimer;
	public boolean GivenShrine;
	public final static String PlayerData = "DoomShrinePlayerData";
	
	public ExtendedPlayerData(EntityPlayer player) {
		this.entplayer = player;
		this.maxTime = ConfigurationHandler.defaultTimeInTicks / 20;
	}
	
	public void copy(ExtendedPlayerData props) {
		entplayer.getDataWatcher().updateObject(ShrineTimer, props.getShrineTimer());
		this.ShrineTimer = props.ShrineTimer;
		this.GivenShrine = props.GivenShrine;
	}
	
	public static final void register(EntityPlayer player) {
		player.registerExtendedProperties(ExtendedPlayerData.PlayerData, new ExtendedPlayerData(player));
		ExtendedPlayerData prop = ExtendedPlayerData.get(player);
		prop.resetShrineTimer();
	}

	public static final ExtendedPlayerData get(EntityPlayer player) {
		return (ExtendedPlayerData) player.getExtendedProperties(PlayerData);
	}
	
	@Override
	public void saveNBTData(NBTTagCompound compound) {
		
		NBTTagCompound properties = new NBTTagCompound();
		
		properties.setInteger("ShrineTimer", this.ShrineTimer);
		properties.setBoolean("GivenShrine", this.GivenShrine);
		
		compound.setTag(PlayerData, properties);
		
	}

	@Override
	public void loadNBTData(NBTTagCompound compound) {
		
		NBTTagCompound properties = (NBTTagCompound) compound.getTag(PlayerData);
		
		this.ShrineTimer = properties.getInteger("ShrineTimer");
		this.GivenShrine = properties.getBoolean("GivenShrine");
	}

	@Override
	public void init(Entity entity, World world) {}
	
	/**
	 * Decreases the Timer in Seconds
	 */
	public final boolean decreaseShrineTimer(int amount) {
		boolean sufficient = amount <= this.ShrineTimer;
		ShrineTimer -= (sufficient ? amount : this.ShrineTimer);
		return sufficient;
	}
	
	/**
	 * Resets the Players Doom Shrine Timer
	 */
	public final void resetShrineTimer() {
		this.ShrineTimer = this.maxTime;
	}

	/**
	 * Returns current  Doom Shrine Timer Value
	 */
	public final int getShrineTimer() {
		return this.ShrineTimer;
	}

	/**
	 * Sets current Time to amount or maxTime if exceeded
	 */
	public final void setShrineTimer(int amount) {
		this.ShrineTimer = (amount > 0 ? (amount < maxTime ? amount : maxTime) : 0);
	}

}
